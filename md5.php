<?php

function _md5($str, $raw = false) {

	$a0 = 0x67452301;
	$b0 = 0xefcdab89;
	$c0 = 0x98badcfe;
	$d0 = 0x10325476;

	$s1 = array(7, 12, 17, 22);
	$s2 = array(5, 9, 14, 20);
	$s3 = array(4, 11, 16, 23);
	$s4 = array(6, 10, 15, 21);
	$s = array_merge(
		$s1, $s1, $s1, $s1,
		$s2, $s2, $s2, $s2,
		$s3, $s3, $s3, $s3,
		$s4, $s4, $s4, $s4
	);

	for ($i = 0; $i < 64; $i++) {
		$k[$i] = floor(abs(sin($i + 1)) * pow(2, 32));
	}

	$bytes = strlen($str);
	$pad = 56 - ($bytes + 1) % 64;
	$pad = $pad >= 0 ? $pad : (64 - abs($pad));
	$str .= "\x80".str_repeat("\x00", $pad);
	$str .= pack('VV', $bytes * 8, 0);

	$chunks = str_split($str, 64);
	foreach ($chunks as $chunk) {
		$words = array_values(unpack('V16', $chunk));
		$a = $a0;
		$b = $b0;
		$c = $c0;
		$d = $d0;
		for ($i = 0; $i < 64; $i++) {
			if ($i < 16) {
				$f = ($b & $c) | ((~ $b) & $d);
				$g = $i;
			} elseif ($i >= 16 && $i < 32) {
				$f = ($b & $d) | ($c & (~ $d));
				$g = ($i *5 + 1) % 16;
			} elseif ($i >= 32 && $i < 48) {
				$f = $b ^ $c ^ $d;
				$g = ($i * 3 + 5) % 16;
			} else {
				$f = $c ^ ($b | (~ $d));
				$g = ($i * 7) % 16;
			}
			$m = $words[$g];
			$a = ($a + $f + $m + $k[$i]) & 0xffffffff;
			$a = lrot($a, $s[$i]);
			$a = ($a + $b) & 0xffffffff;
			list($a, $b, $c, $d) = array($d, $a, $b, $c);
		}
		$a0 = ($a0 + $a) & 0xffffffff;
		$b0 = ($b0 + $b) & 0xffffffff;
		$c0 = ($c0 + $c) & 0xffffffff;
		$d0 = ($d0 + $d) & 0xffffffff;
	}
	$hash = pack('V*', $a0, $b0, $c0, $d0);
	if (!$raw) list(, $hash) = unpack('H*', $hash);
	return $hash;
}

function lrot($a, $b) {
    return ($a << $b) | ($a >> (32 - $b)) & ~(-1 << $b);
}
