<?php

require __DIR__.'/md5.php';

$c = 10;

for ($i = 0; $i < $c; $i++) {
	$rand = fread($f = fopen('/dev/urandom', 'r'), mt_rand(1, 10000));
	assert(md5($rand) === _md5($rand));
	assert(md5($rand, 1) === _md5($rand, 1));
	fclose($f);
}
